#include "image.h"

#include <stdint.h>
#include <stdlib.h>

struct image
create_image(uint64_t const width, uint64_t const height)
{
  struct image image = { .height = height,
                         .width = width,
                         .data =
                           malloc(width * height * sizeof(struct pixel) + 64) };
  return image;
}

void
free_image(struct image* image)
{
  if (image->data) {
    free(image->data);
  }
}
