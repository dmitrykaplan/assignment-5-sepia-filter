.intel_syntax noprefix
.section .text
.global	sepia_sse
.type	sepia_sse, @function
sepia_sse:
  #vzeroall # just 3 bytes c5 fc 77
  #pxor xmm0, xmm0
  #pxor xmm1, xmm1
  #pxor xmm2, xmm2
  #pxor xmm3, xmm3 # blue
  #pxor xmm4, xmm4 # green
  #pxor xmm5, xmm5 # red
  movdqa xmm0, .sepia_blue
  movdqa xmm1, .sepia_green
  movdqa xmm2, .sepia_red
  movdqa xmm7, .color_halfsum

	push	rbp
	push	rbx

  movq rax, [rsp+0x28]

	movq rcx, QWORD PTR [rsp+0x18]
	imul rcx, QWORD PTR [rsp+0x20]
	lea rcx, [rcx+rcx*2]

  xor rdx, rdx

.jump1:
  cmp rcx, rdx
  jbe .jump2
  add rax, 0x0c
  add rdx, 0x0c

  pxor xmm3,xmm3
  pxor xmm4,xmm4
  pxor xmm5,xmm5

  #0x0, 0x4, 0x8, 0xc

  #0x3, 0x6, 0x9, 0xc
  pinsrb xmm3,BYTE PTR [rax-0xc],0x0
  pinsrb xmm3,BYTE PTR [rax-0x9],0x4
  pinsrb xmm3,BYTE PTR [rax-0x6],0x8
  pinsrb xmm3,BYTE PTR [rax-0x3],0xc

  #0x2, 0x5, 0x8, 0xb
  pinsrb xmm4,BYTE PTR [rax-0xb],0x0
  pinsrb xmm4,BYTE PTR [rax-0x8],0x4
  pinsrb xmm4,BYTE PTR [rax-0x5],0x8
  pinsrb xmm4,BYTE PTR [rax-0x2],0xc

  #0x1, 0x4, 0x7, 0xa
  pinsrb xmm5,BYTE PTR [rax-0xa],0x0
  pinsrb xmm5,BYTE PTR [rax-0x7],0x4
  pinsrb xmm5,BYTE PTR [rax-0x4],0x8
  pinsrb xmm5,BYTE PTR [rax-0x1],0xc

  paddd xmm3, xmm4
  paddd xmm3, xmm5

  divps xmm3, xmm7

  movdqa xmm4, xmm3
  movdqa xmm5, xmm3
  mulps xmm3, xmm0
  mulps xmm4, xmm1
  mulps xmm5, xmm2

  #0x3, 0x6, 0x9, 0xc
  pextrb BYTE PTR [rax-0xc],xmm3,0x0
  pextrb BYTE PTR [rax-0x9],xmm3,0x4
  pextrb BYTE PTR [rax-0x6],xmm3,0x8
  pextrb BYTE PTR [rax-0x3],xmm3,0xc

  #0x2, 0x5, 0x8, 0xb
  pextrb BYTE PTR [rax-0xb],xmm4,0x0
  pextrb BYTE PTR [rax-0x8],xmm4,0x4
  pextrb BYTE PTR [rax-0x5],xmm4,0x8
  pextrb BYTE PTR [rax-0x2],xmm4,0xc

  #0x1, 0x4, 0x7, 0xa
  pextrb BYTE PTR [rax-0xa],xmm5,0x0
  pextrb BYTE PTR [rax-0x7],xmm5,0x4
  pextrb BYTE PTR [rax-0x4],xmm5,0x8
  pextrb BYTE PTR [rax-0x1],xmm5,0xc

  jmp .jump1
  

.jump2:
  #vzeroall
	pop	rbx
	pop	rbp
	ret

.section .rodata
.align 16
.sepia_red:
.value	122
.value	122
.value	122
.value	122
.value	122
.value	122
.value	122
.value	122

.align 16
.sepia_green:
.value	66
.value	66
.value	66
.value	66
.value	66
.value	66
.value	66
.value	66

.align 16
.sepia_blue:
.value	20
.value	20
.value	20
.value	20
.value	20
.value	20
.value	20
.value	20

.align 16 
.color_halfsum:
.value 382
.value 382
.value 382
.value 382
.value 382
.value 382
.value 382
.value 382
