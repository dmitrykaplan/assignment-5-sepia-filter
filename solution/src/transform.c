#include "../include/transform.h"
#include "stdio.h"

struct image
rotate(struct image const source)
{
  struct image result = create_image(source.height, source.width);
  for (size_t y = 0; y < result.height; y++) {
    for (size_t x = 0; x < result.width; x++) {
      result.data[y * result.width + x] =
        source.data[(source.height - x) * source.width + y - source.width];
    }
  }
  return result;
}

uint32_t
min(uint32_t x, uint32_t y)
{
  if (x >= y)
    return y;
  else
    return x;
}

uint8_t
get_color(uint8_t base_value, float luma)
{
  float averager =
    300.0f *
    (3.0f / 2.0f); // first value can be changed to achieve desired brightness
  float normalized_luma = luma / averager;
  float base_value_float = (float)base_value;
  base_value_float *= normalized_luma;
  uint32_t int_value = (uint32_t)base_value_float;
  uint8_t trimmed_value = (uint8_t)min(255, int_value);
  return trimmed_value;
}

uint8_t
get_blue(float luma)
{
  // RGB blue value of sepia is 20
  return get_color(20, luma);
}

uint8_t
get_green(float luma)
{
  // RGB green value of sepia is 66
  return get_color(66, luma);
}

uint8_t
get_red(float luma)
{
  // RGB red value of sepia is 122
  return get_color(122, luma);
}

uint32_t
read_luma(uint8_t* address)
{
  uint32_t value = 0;
  value += address[0];
  value += address[1];
  value += address[2];
  return value;
}

struct image
sepia(struct image const source)
{
  struct image result = create_image(source.width, source.height);
  for (size_t y = 0; y < result.height; y++) {
    for (size_t x = 0; x < result.width; x++) {
      uint32_t luma = 0;
      luma =
        read_luma((((uint8_t*)(source.data)) + ((y * result.width + x) * 3)));

      (((uint8_t*)(result.data))[((y * result.width + x) * 3) + 0]) =
        get_blue(luma);
      (((uint8_t*)(result.data))[((y * result.width + x) * 3) + 1]) =
        get_green(luma);
      (((uint8_t*)(result.data))[((y * result.width + x) * 3) + 2]) =
        get_red(luma);
    }
  }
  return result;
}
