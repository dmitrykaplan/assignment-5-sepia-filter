#include "image.h"
#include "stdio.h"

enum read_status
{
  READ_OK = 0,
  READ_UNSUCCESSFUL,
  READ_NOT_BMP,
  READ_BMP_NOT_SUPPORTED
};

enum write_status
{
  WRITE_OK = 0,
  WRITE_ERROR
};

enum read_status
from_bmp(FILE* file, struct image* img);

enum write_status
to_bmp(FILE* file, const struct image* img);
