#include "image.h"

struct image
rotate(struct image const source);

struct image
sepia(struct image const source);

void
sepia_sse(struct image source);
