NAME := image-transformer

CC = gcc
LINKER = $(CC)

RM = rm -rf
MKDIR = mkdir -p

INCLUDE := ./solution/include
SRC     := ./solution/src
SRCS    := $(wildcard $(SRC)/*.c) $(wildcard $(SRC)/*.s)
EXE     := $(BIN)/$(NAME)
CFLAGS  := -no-pie -I$(INCLUDE)

all:
	$(CC) $(CFLAGS) -o $(NAME) $(SRCS)

clean:
	rm $(NAME)
